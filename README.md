# Lazy scripts

My daily work as a Tor Browser developer involves several tasks that can be
automated with some scripts to save a lot of time.

I have several ones now and I think they could be useful also to other members
of the team, therefore I decided to create this repository.

They are not production ready, sometimes they have been used just a couple of
times, and then I have fixed their outcome without further improving the script.
Use at your own risk!
