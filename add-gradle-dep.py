#!/usr/bin/env python3
# A quick&dirty script to add missing dependencies from a debug shell
# during a build.
import hashlib
from pathlib import Path
import re
import sys

import requests


# maven.mozilla.org does not work over IPv6 from our build servers.
# See https://gitlab.torproject.org/tpo/tpa/team/-/issues/41654.
requests.packages.urllib3.util.connection.HAS_IPV6 = False

out = Path("/var/tmp/dist/gradle-dependencies/")


def try_host(host, what):
    url = host + what
    r = requests.get(url)
    dest = out / what
    if r.status_code != 404:
        print(f"Found {url}")
        dest.parent.mkdir(exist_ok=True, parents=True)
        with dest.open("wb") as f:
            f.write(r.content)
        sha = hashlib.sha256(r.content).hexdigest()
        with open("/tmp/additions.txt", "a+") as f:
            f.write(f"{sha} | {url}\n")
        return True


def try_dl(what):
    dest = out / what
    if dest.exists():
        print(f"{what} already downloaded.")
    hosts = [
        "https://maven.mozilla.org/maven2/",
        "https://maven.google.com/",
        "https://plugins.gradle.org/m2/",
        "https://repo1.maven.org/maven2/",
        "https://repo.maven.apache.org/maven2/",
    ]
    for h in hosts:
        if try_host(h, what):
            return
    print(f"{what} not found.")


if len(sys.argv) > 1:
    try_dl(sys.argv[1])
else:
    with open("/tmp/failure.txt") as f:
        files = re.findall(
            r"file:/var/tmp/dist/gradle-dependencies/(\S+)", f.read()
        )
    for f in files:
        try_dl(f)
