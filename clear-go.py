#!/usr/bin/env python3
# Remove old Go artifacts. Run it from a tor-browser-build clone.
from collections import namedtuple
from glob import glob
import os
import re

Artifact = namedtuple("Artifact", ["fn", "minor", "patch"])

pattern = re.compile(r"^go-?1\.([0-9]{1,2})\.?([0-9]{0,2})")
os.chdir("out/go")
artifacts = []
max_vers = {}
for fn in glob("*"):
    m = pattern.match(fn)
    minor = int(m.group(1))
    patch = m.group(2)
    if patch:
        patch = int(patch)
    else:
        patch = 0
    if minor == 4 and patch == 3:
        continue
    if minor < 20:
        os.unlink(fn)
    artifacts.append(Artifact(fn, minor, patch))
    if minor not in max_vers:
        max_vers[minor] = patch
    elif max_vers[minor] < patch:
        max_vers[minor] = patch
for a in artifacts:
    if a.patch < max_vers[a.minor]:
        print(f"Deleting {a.fn}")
        os.unlink(a.fn)
    else:
        print(f"Keeping {a.fn}")
