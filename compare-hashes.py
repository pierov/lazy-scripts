#!/usr/bin/env python3
# Check if the hashes of the local build match with the hashes of
# someone who built on the build servers.
# This script should be run from the root of a tor-browser-build clone.
from pathlib import Path
import sys

import requests

version = sys.argv[1]
channel = "alpha" if "a" in version else "release"


def download_hashes(browser, incrementals):
    builder = sys.argv[2]
    template = "https://tb-build-0{}.torproject.org/~{}/builds/{}browser/{}/unsigned/{}/sha256sums-unsigned-build{}.txt"
    args = [
        2,
        builder,
        browser,
        channel,
        version,
        ".incrementals" if incrementals else "",
    ]
    r = requests.get(template.format(*args))
    if r.status_code == 200:
        return r.text
    args[0] = 3
    r = requests.get(template.format(*args))
    if r.status_code == 200:
        return r.text


def check_browser(browser, name):
    output = Path(f"{browser}browser/{channel}/unsigned/{version}")
    if output.exists():
        build = output / "sha256sums-unsigned-build.txt"
        build_remote = download_hashes(browser, False)
        if not build_remote:
            print(f"Failed to download hashes for {name}!")
            return
        with build.open() as f:
            if f.read() == build_remote:
                print(f"{name} matches")
            else:
                print(f"{name} doesn't match!")
        incr = output / "sha256sums-unsigned-build.incrementals.txt"
        if incr.exists():
            incr_remote = download_hashes(browser, True)
            if not incr_remote:
                print(f"Failed to download hashes for {name} (incrementals)!")
                return
            with incr.open() as f:
                if f.read() == incr_remote:
                    print(f"{name} incrementals match")
                else:
                    print(f"{name} incrementals don't match")


if __name__ == "__main__":
    check_browser("tor", "TBB")
    check_browser("mullvad", "MB")
