# Create multiple DMGs at once with the create-dmg tool on macOS.

set -e

typeset -A bgs
bgs[Base]="background.tiff"
bgs[Tor]="dmg-window_2x.png"
bgs[Mullvad]="background.png"

for b in Base Tor Mullvad; do
  for ch in Alpha Nightly; do
    name="$b Browser $ch"
    bundle="$name.app"
    background=${bgs[$b]}
    mkdir -p "tmp/$bundle"
    create-dmg --volname "$name" \
      --volicon VolumeIcon.icns \
      --background "$background" \
      --hide-extension "$bundle" \
      --window-size 510 342 \
      --icon-size 114 \
      --text-size 12 \
      --icon "$bundle" 111 184 \
      --app-drop-link 399 184 \
      "$name.dmg" tmp
    rm -Rf tmp
  done
done
