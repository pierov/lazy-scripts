#!/bin/bash
# Build incremental updates, sign all the mar files, generate the update
# responses and deploy everything.
# This script should be run from the root of tor-browser-build in the machine
# where you want to deploy the updates (it will generate the URL from its
# hostname).
# Also, it needs you to have a mar private key in ../nssdb and to have
# extracted working mar tools in tmp.
set -e

if [[ -z "$1" ]]; then
  browser="torbrowser"
else
  browser="$1"
fi

if [[ -z "$2" ]]; then
  channel="nightly"
else
  # TODO: Implement unsigned for the other channels
  channel="$2"
fi

version=$(rbm/rbm showconf release var/torbrowser_version --target "$channel")
host=$(hostname -f)
user=$(whoami)
mar_tools=$PWD/tmp/mar-tools
nssdb=$PWD/../nssdb

base_url="https://$host/~$user/$browser/$channel"
if [[ "$browser" = "mullvadbrowser" ]]; then
  resp_dir="updater-mb"
else
  resp_dir="update_3"
fi

# FIXME: For non-nightly, we might need to append unsigned.
# TODO: Detect when we need to also create incrementals.
# make "$browser-incrementals-$channel"
rbm/rbm build release --step update_responses_config --target "$channel" --target "$browser"

pushd "$browser/$channel/$version"
for mar_file in *.mar; do
  # We can always strip signatures
  "$mar_tools/signmar" -r "$mar_file" "$mar_file.unsigned"
done
for mar_file in *.mar.unsigned; do
  "$mar_tools/signmar" -d "$nssdb" -n marsigner -s "$mar_file" "$(basename "$mar_file" .unsigned)"
  echo "Signed $mar_file"
done
popd

pushd tools/update-responses
sed -i -e "s|https://cdn.mullvad.net/browser|$base_url|" config.yml
sed -i -e "s|https://dist.torproject.org/torbrowser|$base_url|" config.yml
sed -i -e "s|https://cdn.torproject.org/aus1/torbrowser|$base_url|" config.yml
./update_responses "$channel"
cp -r "htdocs/$channel" "$HOME/public_html/$resp_dir/"
popd
