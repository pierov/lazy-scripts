# This script is the equivalent of the deploy.sh we have on
# tools/torbrowser in tor-browser.git but for Windows.
# But since we are cross-compiling, it downloads a tarball from some
# server  you can specify at line 15.
# You can create that file with package-windows.sh.
from pathlib import Path
from io import BytesIO
import tarfile
from urllib.request import urlopen

# This archive file should include the content of the firefox
# directory, without the firefox directory itself.
# We use a tarball even though zip is more Windows friendly so that we
# can stream it.
url = "https://.../firefox.tar"

def print_filter(member, path):
    # Give a feedback on what's happening
    print(f"Extracting {member.name}")
    return tarfile.data_filter(member, path)


# Use urlopen instead of requests because we want to use only the
# standard library.
with urlopen(url) as f:
    with tarfile.open(mode="r|", fileobj=f) as t:
        t.extractall(
            path=(Path(__file__).parent / "Browser"), filter=print_filter
        )
