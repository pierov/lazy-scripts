#!/usr/bin/env python3
import argparse
from collections import namedtuple
import multiprocessing
import re
import subprocess


Commit = namedtuple("Commit", ["hash", "subject"])
CommitGroup = namedtuple(
    "CommitGroup", ["subject", "commits", "range"], defaults=[""]
)


def call_git(args, text=True, check=True):
    return subprocess.run(
        ["git"] + args, capture_output=True, text=text, check=check
    ).stdout


def rev_parse(range_or_commit):
    return call_git(["rev-parse", range_or_commit])


def make_range(range_or_commit):
    forced_range = rev_parse(range_or_commit).split("\n")
    if len(forced_range) == 1:
        return f"{rev_parse}^!"
    else:
        return range_or_commit


def make_diff(range_or_commit, denoise=False):
    forced_range = make_range(range_or_commit)
    # My first attempt was to call git diff --name-only, but it was
    # quite slow, because it needed to run a new process for each diff,
    # and it seemed very, very slow.
    # This approach faster, but more delicate.
    raw_diffs = call_git(["diff", forced_range], text=False).split(b"\n")
    diff_groups = []
    current_diff = []
    for line in raw_diffs:
        if line.startswith(b"diff --git"):
            diff_groups.append(current_diff)
            current_diff = []
        if denoise:
            if line.startswith(b"index "):
                continue
            elif line.startswith(b"@@ "):
                line = b"@@ "
        current_diff.append(line)
    diff_groups.append(current_diff)
    # Remove the first empty group.
    diff_groups.pop(0)

    # We could probably go through the full raw diff only once, but
    # dividing into files first helped me solving unexpected problems
    # (in particular, the new empty file case).
    diffs = {}
    binaries = set()
    for group in diff_groups:
        binary = False
        old_file = ""
        new_file = ""
        for line in group:
            binary_files = b"Binary files "
            if line.startswith(b"+++ "):
                new_file = line[4:].decode("utf-8").strip()
                if new_file.startswith("b/"):
                    new_file = new_file[2:]
                # After this we should have the actual diff, which we
                # are not interested in parsing.
                break
            elif line.startswith(b"--- "):
                old_file = line[4:].decode("utf-8").strip()
                if old_file.startswith("a/"):
                    old_file = old_file[2:]
            elif line.startswith(binary_files):
                binary = True
                s = line.decode("utf-8")
                # This pattern isn't very strong, but hopefully it's
                # enough for us...
                and_dev_null = " and /dev/null differ"
                if s.endswith(and_dev_null):
                    new_file = "/dev/null"
                    old_file = s[len(binary_files) + 3 : -len(and_dev_null)]
                    binaries.add(old_file)
                else:
                    new_file = re.findall(r" and b/(.*) differ$", s)[0].strip()
                    binaries.add(new_file)
                break
        if not new_file:
            if group[1].startswith(b"new file mode "):
                # New empty files do not have a diff.
                # Rely on the command to find the file name.
                command = group[0].decode("utf-8")
                m = re.match("diff --git a/(.*) b/(.*)$", command)
                assert m and m.group(1) == m.group(2)
                new_file = m.group(1)
            elif group[-1].startswith(b"rename to "):
                new_file = group[-1][10:].decode("utf-8")
            if not new_file:
                raise Exception("Empty new_file. Group content: " + group)
        if new_file == "/dev/null":
            diffs[old_file] = f"{old_file} removed".encode("utf-8")
        elif not binary:
            diffs[new_file] = b"\n".join(group) + b"\n"

    return (diffs, binaries)


def write_outs(diffs, file_or_filename):
    if issubclass(type(file_or_filename), str):
        with open(file_or_filename, "wb") as f:
            write_outs(diffs, f)
    else:
        names = sorted(diffs.keys())
        for name in names:
            file_or_filename.write(diffs[name])


def get_commit_groups(maybe_range):
    log = call_git(["log", "--format=%H %s", make_range(maybe_range)])
    # Remove the final empty line
    log = log.split("\n")[:-1]
    log.reverse()
    groups = []
    for line in log:
        # Alternative: do not even include the space, and just use the
        # correct offsets.
        s = line.split(" ", 1)
        (hash_, subj) = (s[0], s[1])
        if not groups or not subj.endswith(groups[-1].subject):
            # It seems namedtuple's defaults shares the same object?
            groups.append(CommitGroup(subj, []))
        groups[-1].commits.append(Commit(hash_, subj))
    for i, g in enumerate(groups):
        r = g.commits[0].hash + f"^..{g.commits[-1].hash}"
        groups[i] = g._replace(range=r)
    return groups


def diff_of_diffs(range1, range2, denoise):
    diffs_a, binaries_a = make_diff(range1, denoise)
    diffs_b, binaries_b = make_diff(range2, denoise)

    output_a = {}
    output_b = {}
    no_diffs = True
    for f, diff in diffs_a.items():
        if f not in diffs_b or diffs_b[f] != diff:
            output_a[f] = diff
            no_diffs = False
    for f, diff in diffs_b.items():
        if f not in diffs_a or diffs_a[f] != diff:
            output_b[f] = diff
            no_diffs = False
    binaries = binaries_a | binaries_b
    last_a = rev_parse(range1).split("\n")[0]
    last_b = rev_parse(range2).split("\n")[0]
    for f in binaries:
        diff = call_git(["diff", last_a, last_b, "--", f], text=False)
        # The output of diff is just \n if the files are the same.
        if diff.strip():
            output_a[f] = diff
            output_b[f] = diff
            no_diffs = False
    return output_a, output_b, no_diffs


def _range_diff_cb(args):
    (g1, g2), denoise = args
    return diff_of_diffs(g1.range, g2.range, denoise)


def range_diff(range1, range2, out1, out2, denoise):
    groups1 = get_commit_groups(range1)
    groups2 = get_commit_groups(range2)
    matches = []

    def filter_cb(g1):
        for i, g2 in enumerate(groups2):
            # This might fail in some cases (e.g., fixups with the same
            # name).
            if g1.subject == g2.subject:
                matches.append((g1, g2))
                groups2.pop(i)
                # Matched, filter it out
                return False
        return True

    groups1 = list(filter(filter_cb, groups1))

    # We cannot use shortcuts, and need a `git diff` for each commit
    # group. However, they can all be run in parallel, so let's do it.
    # My initial implementation without this trick took more than one
    # minute to run on the whole patchset... But it had some but with
    # blocks with only one commit (they were not forced into ranges
    # correctly). Anyway, since I implemented this, I didn't want to
    # revert it.
    with multiprocessing.Pool() as p:
        group_diffs = p.map(_range_diff_cb, [(m, denoise) for m in matches])
    no_diffs_at_all = len(groups1) == 0 and len(groups2) == 0

    def write_commits(f, g):
        for c in g.commits:
            f.write(f"{c.hash} {c.subject}\n".encode("utf-8"))

    def write_group(f, i, g, d, nd):
        f.write(80 * b"-")
        f.write(f"\nGroup {i}\n".encode("utf-8"))
        write_commits(f, g)
        if nd:
            f.write(b"\tNo diffs")
        else:
            f.write(b"\n")
            write_outs(d, f)
        f.write(b"\n\n\n")

    def write_unmatched(f, groups):
        f.write(80 * b"-")
        f.write(b"\nUnmatched groups:\n")
        for g in groups:
            write_commits(f, g)
            f.write(b"\n")

    with open(out1, "wb") as fa, open(out2, "wb") as fb:
        for (i, (g1, g2)), (output_a, output_b, no_diffs) in zip(
            enumerate(matches), group_diffs
        ):
            write_group(fa, i, g1, output_a, no_diffs)
            write_group(fb, i, g2, output_b, no_diffs)
            no_diffs_at_all = no_diffs_at_all and no_diffs
        if groups1:
            write_unmatched(fa, groups1)
        if groups2:
            write_unmatched(fb, groups2)

    if no_diffs_at_all:
        print("No diff at all found, yay! 🎉️")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m", "--mode", choices=["careful", "denoise"], default="denoise"
    )
    parser.add_argument(
        "--range-diff", "--rd", action=argparse.BooleanOptionalAction
    )
    parser.add_argument("first_range")
    parser.add_argument("second_range")
    parser.add_argument("first_output")
    parser.add_argument("second_output")
    args = parser.parse_args()

    denoise = args.mode == "denoise"

    if args.range_diff:
        range_diff(
            args.first_range,
            args.second_range,
            args.first_output,
            args.second_output,
            denoise,
        )
        return

    output_a, output_b, no_diffs = diff_of_diffs(
        args.first_range, args.second_range, denoise
    )
    write_outs(output_a, args.first_output)
    write_outs(output_b, args.second_output)
    if no_diffs:
        print("No diff found, yay! 🎉️")


if __name__ == "__main__":
    main()
