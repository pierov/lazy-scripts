#!/usr/bin/env python3
# This script should be run in a container after an online build of
# something that uses Gradle (GeckoView, Android Components, Fenix,
# etc...).
# The idea is to let Gradle work normally, and then we copy all the
# file it downloaded in /var/tmp/dist/android-toolchain/gradle/caches
# (which this script assumes it has been renamed to caches___online)
# to a directory in a format that looks like the directories we create
# with the downloaded artifacts.
# Then we run an offline build using that directory as a source to make
# sure we have not missed anything in the process.
from collections import namedtuple
from pathlib import Path
from shutil import copyfile


Artifact = namedtuple(
    "Artifact", ["path", "group_id", "artifact_id", "version", "sha1", "file"]
)
dest = Path("/var/tmp/dist/gradle-dependencies")
old_dir = Path(
    "/var/tmp/dist/android-toolchain/gradle/caches/modules-2/files-2.1/"
)
artifacts = []
for path in old_dir.rglob("*"):
    # We assume we need all the files, so we don't either check the
    # suffix.
    if not path.is_file():
        continue
    rel = str(path.relative_to(old_dir))
    artifacts.append(Artifact(*([path] + rel.split("/"))))

for a in artifacts:
    dest_dir = dest / a.group_id.replace(".", "/") / a.artifact_id / a.version
    dest_dir.mkdir(parents=True, exist_ok=True)
    copyfile(a.path, dest_dir / a.file)
