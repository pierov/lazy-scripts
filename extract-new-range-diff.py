#!/usr/bin/env python3
"""Script to filter a range diff to print only the commits that have
been added. Should be piped to git range-diff.
"""
import sys

PREFIX = "  -:  ------------- >"

while True:
    line = sys.stdin.readline()
    if not line:
        break
    line = line.rstrip("\n")
    if line.startswith(PREFIX):
        print("- [ ]" + line[len(PREFIX) :])
