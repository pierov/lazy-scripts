#!/usr/bin/env python3
import sys

from git import Repo

r = Repo()
files = list(r.index.unmerged_blobs().keys())

conflict_start = b"<<<<<<< HEAD\n"
conflict_end = b"=======\n"
invalid_sha = "0000000000000000000000000000000000000000"

if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} last-good-base", file=sys.stderr)
    sys.exit(1)

last_good = r.commit(sys.argv[1])

conflicts = set()
for fn in files:
    conflicts_file = set()
    with open(fn, "rb") as f:
        lines = f.readlines()
        end = 0
        while True:
            try:
                start = lines.index(conflict_start, end)
            except ValueError:
                break
            end = lines.index(conflict_end, start)
            # Sum 1 to skip the armature, and 1 because lines start
            # from 1.
            # For the end, we want to skip the end armature, so we do
            # not subtract anything.
            for c in r.blame(None, fn, L=f"{start+2},{end}"):
                if c[0].hexsha == invalid_sha:
                    print(f"{fn} contains an empty conflict!")
                else:
                    conflicts.add(c[0])
                    conflicts_file.add(c[0])
    print(f"Conflicts for {fn}:")
    for c in conflicts_file:
        if c.committed_datetime > last_good.committed_datetime:
            print(f"\t{c} {c.committed_datetime}")

if not conflicts:
    print("No conflicts?!")
    sys.exit(1)

conflicts = list(conflicts)
oldest = None
for c in conflicts:
    if c.committed_datetime > last_good.committed_datetime and (
        oldest is None or c.committed_datetime < oldest.committed_datetime
    ):
        oldest = c
print(oldest, oldest.committed_datetime)
