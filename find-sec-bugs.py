#!/usr/bin/env python3
# Parse the output of `git log --oneline` to find which commits are
# associated to confidential Bugzilla tickets and therefore are
# possible backport candidates. 
import re

import requests

# TODO: Invoke `git log` directly.
with open("bugs.txt") as f:
    for line in f.readlines():
        m = re.match("^[0-9a-f]+\sBug ([0-9]+)", line)
        if not m:
            continue
        num = m.group(1)
        url = f"https://bugzilla.mozilla.org/show_bug.cgi?id={num}"
        r = requests.get(url)
        if r.text.find("<title>Access Denied</title>") != -1:
            print(url)
