#!/usr/bin/env python3
from pathlib import Path
import ruamel.yaml

yaml = ruamel.yaml.YAML()
yaml.indent(mapping=2, sequence=4, offset=2)
yaml.width = 4096
yaml.preserve_quotes = True

for proj in Path("projects").glob("*"):
    try:
        config_path = proj / "config"
        config_yaml = yaml.load(config_path)
        with config_path.open("w") as f:
            yaml.dump(config_yaml, f)
    except Exception as e:
        print(f"Failed to process {proj}", e)
