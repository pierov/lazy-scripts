#!/bin/bash
# Call `git show CHERRY_PICK_HEAD -- $conflicting_files`.
repo=$(git rev-parse --show-toplevel)
cd $repo

git rev-parse CHERRY_PICK_HEAD >/dev/null 2>&1
have_cherry_pick=$?
git rev-parse REBASE_HEAD >/dev/null 2>&1
have_rebase=$?

if [[ $have_cherry_pick -eq 0 ]]; then
  head=CHERRY_PICK_HEAD
elif [[ $have_rebase -eq 0 ]]; then
  head=REBASE_HEAD
else
  echo "Not rebasing and not cherry-picking?"
  exit 1
fi

conflicts=$(git status --porcelain=v2 | grep -E '^u ' | awk '{print $11}')
git show $head -- $conflicts
