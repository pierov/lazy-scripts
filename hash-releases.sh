#!/bin/bash

version=$(rbm/rbm showconf release var/torbrowser_version)
build=$(rbm/rbm showconf release var/torbrowser_build)
if [[ "$version" == *a* ]]; then
  channel=alpha
else
  channel=release
fi

rel_dir="$channel/unsigned/$version-$build"
tbb_dir="torbrowser/$rel_dir"
mb_dir="mullvadbrowser/$rel_dir"

if [[ -d "$tbb_dir" ]]; then
  sha256sum "$tbb_dir"/*.txt
fi
if [[ -d "$mb_dir" ]]; then
  sha256sum "$mb_dir"/*.txt
fi
