#!/usr/bin/env python3
# This script is intended to be run inside a container, after we have
# updated /var/tmp/gradle-dependencies (e.g., with
# extract-gradle-artifacts.py) to create a list that looks like the
# gradle-depdencies-list.txt files we include in tor-browser-build.
from hashlib import sha256
from pathlib import Path


dest = Path("/var/tmp/dist/gradle-dependencies")
for path in dest.rglob("*"):
    if not path.is_file():
        continue
    with path.open("rb") as f:
        h = sha256(f.read()).hexdigest()
    rel = str(path.relative_to(dest))
    print(f"{h} | $HOST/{rel}")
