#!/usr/bin/env python3
# This file takes two lists of Gradle dependencies: one that used to
# work (old-list.txt), and another one that has been created with
# list-gradle-artifacts.py (new.txt).
# It tries to match new artifacts against the old ones, if a repository
# hosted an old version, it is likely that it is hosting also a newer
# one. If we do not find a certain artifact we try to look for it on a
# list of known Maven repositories, one by one until we match the
# needed file.
from hashlib import sha256
from urllib.parse import urlparse, urlunparse

import requests


def try_dl(url, expected_sha):
    r = requests.get(url)
    if r.status_code != 200:
        return False
    sha = sha256(r.content).hexdigest()
    if sha != expected_sha:
        print(f"{url} found, but different sha downloaded")
        return False
    return True


old_list = []
with open("old-list.txt") as f:
    old_list = [li.strip().split(" | ") for li in list(f.readlines())[3:]]

available = {}
known_artifacts = {}
for sha, url in old_list:
    u = urlparse(url)
    if u.path.startswith("/maven2/"):
        path = u.path[8:]
    elif u.path.startswith("/m2/"):
        path = u.path[4:]
    else:
        path = u.path[1:]
    available[path] = (sha, url)
    u = u._replace(path=u.path.rsplit("/", 2)[0])
    known_artifacts[path.rsplit("/", 2)[0]] = urlunparse(u)

repos = [
    "https://maven.mozilla.org/maven2/",
    "https://maven.google.com/",
    "https://plugins.gradle.org/m2/",
    "https://repo1.maven.org/maven2/",
    "https://repo.maven.apache.org/maven2/",
]


def try_match(sha, path):
    prefix, version, file = path.rsplit("/", 2)
    if prefix in known_artifacts:
        maybe_url = f"{known_artifacts[prefix]}/{version}/{file}"
        if try_dl(maybe_url, sha):
            print(f"Downloaded {maybe_url}")
            return (sha, maybe_url)
    if path.startswith("maven2/"):
        path = path[len("maven2/") :]
    for repo in repos:
        maybe_url = repo + path
        if try_dl(maybe_url, sha):
            print(f"Downloaded {maybe_url}")
            return (sha, maybe_url)
    print(f"Could not find {path}")


def main():
    with open("new-list.txt") as f:
        new_list = [li.strip().split(" | ") for li in f.readlines()]

    matched = []
    missed = []
    for sha, path in new_list:
        path = path[6:]
        if path.startswith("maven2/"):
            path = path[len("maven2/") :]
        elif path.startswith("m2/"):
            path = path[3:]
        if path in available and available[path][0] == sha:
            matched.append((sha, available[path][1]))
            print(f"Found from old list: {available[path][1]}")
        elif path in available and available[path][0] != sha:
            print(f"Mismatching sha {path}")
        else:
            print("Missing: ", path)
            missed.append((sha, path))

    for sha, path in missed:
        maybe_matched = try_match(sha, path)
        if maybe_matched:
            matched.append(maybe_matched)

    matched = sorted(matched, key=lambda a: a[1])
    with open("output.txt", "w") as f:
        f.write(
            "# On how to update dependencies see doc/how-to-create-gradle-dependencies-list.txt\n"
        )
        f.write(
            "# Don't forget to update var/gradle_dependencies_version when modifying this file\n"
        )
        f.write("sha256sum | url\n")
        for a in matched:
            f.write(f"{a[0]} | {a[1]}\n")


if __name__ == "__main__":
    main()
