#!/bin/bash
# This script can be used to create a tarball to download on Windows and extract
# on top of a Browser directory of an existing installation, like we do with
# deploy.sh on Linux.
set -e
export PATH=/var/tmp/dist/mingw-w64-clang/bin:/var/tmp/dist/fxc2/bin:/var/tmp/dist/rust/bin:/var/tmp/dist/cbindgen:/var/tmp/dist/nasm/bin:/var/tmp/dist/node/bin:$PATH
# Use a tarball because they can be streamed.
rm -f ~/firefox.tar
cd ~/tor-browser
./mach build stage-package

if [[ $* == *--debug* ]]; then
  debug="-C ../ include -C ../toolkit/library/build xul.pdb"
fi

# Firefox build system uses a lot of symlinks!
# Also, no compression because it takes a too long time.
tar -chf ~/firefox.tar -C obj-x86_64-w64-mingw32/dist/firefox . $debug
