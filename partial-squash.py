#!/usr/bin/env python3
import argparse
from pathlib import Path
import re
import sys

import git


parser = argparse.ArgumentParser()
parser.add_argument(
    "-b", "--base", help="The base for the branch (usually a FIREFOX_... tag)."
)
parser.add_argument(
    "-r",
    "--repository",
    default=Path("."),
    help="The path to a Tor Browser/Mullvad Browser clone",
)
parser.add_argument(
    "--no-fetch", action="store_true", help="Do not fetch the remotes."
)
parser.add_argument(
    "new_branch",
    help="The name of the branch to create as a result of this procedure.",
)
parser.add_argument(
    "old_branch",
    default="",
    nargs="?",
    help="The branch to partially squash. "
    "If not provided, the script will try to detect one from the current one.",
)
args = parser.parse_args()

r = git.Repo(args.repository)

if args.old_branch:
    old_branch = args.old_branch
else:
    old_branch = r.git.branch(show_current=True)
if not old_branch:
    print(
        "Cannot find a branch, please provide one explicitly.", file=sys.stderr
    )
    sys.exit(1)
m = re.match(r"(base|tor|mullvad)-browser-([^-]+)-(.+)", old_branch)
if not m:
    print(f"Cannot use {old_branch} as a starting branch.", file=sys.stderr)
    sys.exit(1)
browser = m.group(1)
ff_version = m.group(2)

if args.new_branch in r.branches:
    print(f"{args.new_branch} already exists.", file=sys.stderr)
    sys.exit(1)

tb_remote = None
mb_remote = None
for remote in r.remotes:
    if "tpo/applications/tor-browser" in remote.url:
        tb_remote = remote
    elif "tpo/applications/mullvad-browser" in remote.url:
        mb_remote = remote
if tb_remote is None:
    print(
        "Could not find the upstream remote for tor-browser.git",
        file=sys.stderr,
    )
    sys.exit(1)
if mb_remote is None and browser == "mullvad":
    print(
        "Could not find the upstream remote for mullvad-browser.git",
        file=sys.stderr,
    )
    sys.exit(1)
print(f"Rebasing and squashing {old_branch}.")
old_b1 = old_branch + "-build1"
base_browser = "base" + old_branch[len(browser) :]

if not args.no_fetch:
    print(f"Fetching {tb_remote}")
    tb_remote.fetch()
if browser == "mullvad":
    if not args.no_fetch:
        print(f"Fetching {mb_remote}")
        mb_remote.fetch()
    old_branch = mb_remote.refs[old_branch]
else:
    old_branch = tb_remote.refs[old_branch]

if not args.base:
    try:
        base = r.git.tag(
            f"FIREFOX_{ff_version.replace('.', '_')}_*", list=True
        ).split("\n")[0]
    except:  # noqa
        print("Cannot find the base for the rebase.", file=sys.stderr)
        sys.exit(1)
else:
    base = args.base

base_hash = r.rev_parse(base)
if not base_hash:
    print("Invalid base", file=sys.stderr)
    sys.exit(1)
print(f"Using {base} as a base.")

# base-browser-build1 of course :)
bbb1 = r.tags[base_browser + "-build1"]
new_branch = r.create_head(args.new_branch, bbb1.tag.object)
new_branch.checkout()
print(f"Squashing {base}..{bbb1}")
with r.git.custom_environment(EDITOR="true"):
    r.git.rebase(base_hash, interactive=True, autosquash=True)

fork_point = r.merge_base(tb_remote.refs[base_browser], old_branch)[0]
if fork_point != bbb1.tag.object:
    print(f"The fork point was not {bbb1}, cherry-picking...")
    r.git.cherry_pick(f"{bbb1}..{fork_point.hexsha}")
browser_start = r.head.commit

if browser != "base":
    print(f"Cherry-picking up to {old_b1}...")
    r.git.cherry_pick(f"{fork_point}..{old_b1}")
    with r.git.custom_environment(EDITOR="true"):
        print(f"Squashing from the fork point to {old_b1}")
        r.git.rebase(browser_start, interactive=True, autosquash=True)

    print(f"Cherry-picking {old_b1}..{old_branch}")
    r.git.cherry_pick(f"{old_b1}..{old_branch}")

print("Done!")
