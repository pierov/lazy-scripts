# This file is intended to be sourced in a fish shell before building Tor
# Browser for Android.
# It is not generic at all, and it does not intend to be.
# However, it might help other people to create their version of a similar
# script.

set TOOLCHAINS "$HOME/Tor/toolchains"

set -gx JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64
set -gx ANDROID_HOME $TOOLCHAINS/android-sdk-linux

set -gx PATH "$JAVA_HOME/bin/" $PATH "$ANDROID_HOME/build-tools/31.0.0"

if string match -q "$penv_version" 115
  # Get it from android-components/buildSrc/src/main/java/Gecko.kt
  set -gx MOZ_BUILD_DATE 20230710165010
  set -gx GRADLE_HOME "$TOOLCHAINS/gradle-7.5.1/"
  set -gx ANDROID_NDK_HOME $ANDROID_HOME/ndk/android-ndk-r23c
  set -gx PATH "$TOOLCHAINS/clang-16.0.4/bin" "$TOOLCHAINS/cbindgen-0.24.3" $PATH
else
  set -gx GRADLE_HOME "$TOOLCHAINS/gradle-8.8/"
  set -gx ANDROID_NDK_HOME $ANDROID_HOME/ndk/android-ndk-r26c
  set -gx PATH "$TOOLCHAINS/clang-18.1.5/bin" "$TOOLCHAINS/cbindgen-0.26.0" $PATH
end

set -gx PATH "$GRADLE_HOME/bin" $PATH

# set -gx WASI_SYSROOT "/media/lssd/Tor/wasi-sysroot/"

set -gx TOR_BROWSER_BUILD ~/Tor/tor-browser-build
set -gx LOCAL_DEV_BUILD 1

set -gx MOZ_CHROME_MULTILOCALE "it"
