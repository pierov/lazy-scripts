#!/usr/bin/env python3
# This is a hack to get faster Tor Browser/Mullvad Browser builds.
# It's a work in progress and doesn't build GeckoView/Firefox Android yet.
# However, I would like to hack RBM to output some machine-readable data of
# what it will do, and use it to build a better scheduler.
# This script should be run on the root of a tor-browser-build clone.
import os
from pathlib import Path
import subprocess
import sys
import threading
import time

try:
    import psutil

    has_psutil = True
except:
    # This should be good enough for tb-build-* machines, but feel free
    # to change it.
    GV_SLEEP = 300
    has_psutil = False


def run_rbm(args, targets=[], capture=False):
    args = ["rbm/rbm"] + args
    for t in targets:
        args += ["--target", t]
    proc = subprocess.run(
        args, capture_output=capture, env={"RBM_NO_DEBUG": "1"}
    )
    if capture:
        return proc.stdout.decode("utf-8")


def get_conf(project, key, targets, step=None):
    args = ["showconf", project, key]
    if not step is None:
        args += ["--step", step]
    return run_rbm(args, targets, True).strip()


def should_build(project, targets, step=None):
    filename = get_conf(project, "filename", targets, step)
    return not (Path("out") / project / filename).exists()


def build_if_needed(project, targets, step=None):
    if not should_build(project, targets, step):
        print(f"No need to build {project} ({', '.join(targets)})")
        return
    print(f"Building {project} ({', '.join(targets)})")
    args = ["build", project]
    if step is not None:
        args += ["--step", step]
    run_rbm(args, targets)


def threaded_build(project, targets, step=None):
    t = threading.Thread(
        target=build_if_needed,
        name=project + ":" + ",".join(targets),
        args=(project, targets, step),
    )
    t.start()
    return t


def _desktop_helper(targets, ff_thread):
    ff_thread.join()
    build_if_needed("browser", targets)


def build_firefox(browser, platform, channel):
    targets = [f"{browser}-{platform}", channel]
    is_android = "android" in platform
    project = "geckoview" if is_android else "firefox"
    if not should_build(project, targets):
        if is_android:
            print(f"GeckoView {targets} already up to date")
            return
        else:
            print(
                f"Firefox {targets} already up to date, (maybe) building browser"
            )
            return threaded_build("browser", targets)

    lock = Path(f"git_clones/{project}/.git/index.lock")
    while lock.exists():
        time.sleep(1)

    t = threaded_build(project, targets)
    if has_psutil:
        full_for = 0
        while full_for < 120:
            while psutil.cpu_percent() < 90.0:
                time.sleep(1)
            max_start = time.time()
            while psutil.cpu_percent() >= 90.0:
                full_for = time.time() - max_start
                time.sleep(1)
        print("Firefox is probably linking, starting the next platform")
    else:
        time.sleep(GV_SLEEP)
    if is_android or platform.startswith("macos"):
        return t
    t2 = threading.Thread(target=_desktop_helper, args=(targets, t))
    t2.start()
    return t2


if __name__ == "__main__":
    channel = sys.argv[1]
    subprocess.run(["make", "submodule-update"])
    mullvad = len(sys.argv) >= 3 and sys.argv[2] == "mullvad"
    only_desktop = len(sys.argv) >= 3 and sys.argv[2] == "desktop"
    only_android = len(sys.argv) >= 3 and sys.argv[2] == "android"
    # Define it immediately to start with the source code ASAP
    if mullvad:
        browser = "mullvadbrowser"
    else:
        browser = "torbrowser"
    threads = [
        threaded_build(
            "firefox", [f"{browser}-macos", channel], "src-tarballs"
        )
    ]
    if mullvad:
        desktop = [
            "linux-x86_64",
            "windows-x86_64",
            "macos-x86_64",
            "macos-aarch64",
        ]
    elif only_android:
        desktop = []
    else:
        desktop = [
            "linux-x86_64",
            "linux-i686",
            "windows-x86_64",
            "windows-i686",
            "macos-x86_64",
            "macos-aarch64",
        ]
        threads.append(
            threaded_build("manual", [f"{browser}-linux-x86_64", channel])
        )
        for p in desktop:
            build_if_needed("tor-expert-bundle", [f"{browser}-{p}", channel])
    if not mullvad and not only_desktop:
        build_if_needed(
            "tor-android-service", [f"{browser}-android-armv7", channel]
        )
    for p in desktop:
        t = build_firefox(browser, p, channel)
        if not t is None:
            threads.append(t)

    android = ["armv7", "aarch64", "x86", "x86_64"]
    if not mullvad and not only_desktop:
        for arch in android:
            t = build_firefox(browser, f"android-{arch}", channel)
            if t:
                threads.append(t)
        for t in threads:
            t.join()
        threads = []
        build_if_needed(
            "firefox-android", [f"{browser}-android-armv7", channel]
        )
        for arch in android:
            threads.append(
                threaded_build(
                    "browser", [f"{browser}-android-{arch}", channel]
                )
            )

    for t in threads:
        t.join()
