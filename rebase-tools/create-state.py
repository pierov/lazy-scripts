#!/usr/bin/env python3
import argparse
import json
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("channel")
parser.add_argument("esr_version")
parser.add_argument("browser_series")
parser.add_argument("--browser-rebase", default=1)
parser.add_argument("--state-file", type=Path, default="state.json")
args = parser.parse_args()

suffix = f"{args.esr_version}-{args.browser_series}-{args.browser_rebase}"
major = args.esr_version[:args.esr_version.find(".")]

browsers = ["base-browser", "tor-browser", "mullvad-browser"]
conf = {}
for b in browsers:
    conf[b] = {
        "branch": f"{b}-esr{major}-{args.browser_series}",
        "follows": f"{b}-{suffix}",
        "last-seen": "",
    }
    if b == "base-browser":
        conf[b]["moz-branch"] = f"esr{major}"

with args.state_file.open("r+") as f:
    try:
        existing = json.load(f)
    except:
        existing = {}
    existing[args.channel] = conf
    f.seek(0)
    f.truncate()
    json.dump(existing, f, indent=2)
    f.write("\n")
