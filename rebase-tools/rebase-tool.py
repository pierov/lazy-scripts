#!/usr/bin/env python3
import argparse
from collections import namedtuple
import json
import logging
from pathlib import Path
import sys
from time import time
from uuid import uuid4

import git

logger = logging.getLogger(__name__)

ESR_BRACH = "esr115"


Settings = namedtuple(
    "Settings",
    ["repo_dir", "moz_remote", "tbb_remote", "mb_remote"],
)


def get_remote(settings, browser):
    if browser == "mullvad-browser":
        return settings.mb_remote
    return settings.tbb_remote


def random_branch_name(details):
    if details:
        details = details + "-"
    return f"rebase-tool-{details}{time()}-{uuid4()}"


def cherry_pick(repo, commit):
    try:
        repo.git.cherry_pick(commit)
    except git.exc.GitCommandError as e:
        # Special case: for us an empty commit is not a failure, we
        # will just skip it.
        if not repo.is_dirty() and "--allow-empty" in e.stderr:
            repo.git.cherry_pick("--skip")
            return
        repo.git.cherry_pick("--abort")
        raise


class RebaseTool:

    def __init__(self, settings_file_path, state_file_path):
        with settings_file_path.open() as f:
            self.settings = Settings(**json.load(f))
        logger.debug("Loaded settings: %s", self.settings)
        self.state_path = state_file_path
        self.load_state()
        self.repo = git.repo.Repo(self.settings.repo_dir)
        logger.debug(f"Loaded repository {self.settings.repo_dir}")
        if self.repo.is_dirty():
            logger.warning("The tree is currently dirty...")

    def load_state(self):
        if not self.state_path.exists():
            logger.warning("State file doesn't exist, setting an empty state.")
            self.state = {}
            return
        with self.state_path.open() as f:
            self.state = json.load(f)
            logger.debug("Loaded state", self.state)

    def run(self, fetch=True, push=True):
        if fetch:
            self.fetch()
        for ch in self.state.keys():
            self.run_channel(ch, push)

    def run_channel(self, ch, push=True):
        state = self.state[ch]
        handler = ChannelRebaser(self.repo, self.settings, ch, state)
        handler.run(push)
        self._write_state()

    def fetch(self):
        logger.debug("Fetching the Mozilla remote")
        self.repo.remotes[self.settings.moz_remote].fetch()
        logger.debug("Fetching the Tor Browser remote")
        self.repo.remotes[self.settings.tbb_remote].fetch()
        logger.debug("Fetching the Mullvad Browser remote")
        self.repo.remotes[self.settings.mb_remote].fetch()

    def _write_state(self):
        with self.state_path.open("w") as f:
            json.dump(self.state, f, indent=2)
            logger.debug("Written the state")
            f.write("\n")

    def recreate_on_build1(self, channel=None):
        if channel is None:
            for ch in self.state.keys():
                self.recreate_on_build1(ch)
            return
        for browser in self.state[channel].keys():
            self._recreate_on_build1(browser, channel)

    def _recreate_on_build1(self, browser, channel):
        state = self.state[channel][browser]
        tag = state["follows"] + "-build1"
        logger.info(
            "Starting to recreate %s %s on %s", browser, channel, tag
        )
        if browser == "base-browser":
            moz_branch = self.repo.remotes[self.settings.moz_remote].refs[
                state["moz-branch"]
            ]
            base = self.repo.merge_base(moz_branch, tag)
            if len(base) != 1:
                logger.error("Cannot find a base for base-browser!")
                return
            base = base[0]
            target = moz_branch
        else:
            base = self.state[channel]["base-browser"]["follows"] + "-build1"
            target = self.state[channel]["base-browser"]["branch"]
        logger.debug("Base: %s, target: %s", base, target)
        branch = self.repo.branches[state["branch"]]
        branch.checkout()
        self.repo.git.reset(target, hard=True)
        self.repo.git.cherry_pick(f"{base}..{tag}")
        with self.repo.git.custom_environment(EDITOR="true"):
            first = True
            while True:
                try:
                    if first:
                        first = False
                        self.repo.git.rebase(
                            target, interactive=True, autosquash=True
                        )
                    else:
                        self.repo.git.rebase("--continue")
                    break
                except git.exc.GitCommandError as e:
                    if "--allow-empty" in e.stderr:
                        logger.info("Dropping an empty commit!")
                        self.repo.git.reset("HEAD^")
                    else:
                        raise

        state["last-seen"] = self.repo.tag(tag).commit.hexsha
        self._write_state()

    def reset_to_remote(self, channel=None):
        if not channel:
            for ch in self.state.keys():
                self.reset_to_remote(ch)
            return
        for browser in self.state[channel].values():
            branch = self.repo.branches[browser["branch"]]
            branch.checkout()
            self.repo.git.reset("--hard", branch.tracking_branch().commit)

    def reset_to_build1(self, channel, versions):
        for browser, values in self.state[channel].items():
            values["follows"] = f"{browser}-{versions}"
            values["last-seen"] = self.repo.rev_parse(
                f'{values["follows"]}-build1'
            ).hexsha
        self._write_state()


class ChannelRebaser:

    def __init__(self, repo, settings, channel, state):
        self.repo = repo
        self.settings = settings
        self.channel = channel
        self.state = state

        self.browsers = []
        self.branches = {}
        self.ref_branches = {}
        for b, s in state.items():
            self.branches[b] = self.repo.branches[self.state[b]["branch"]]
            self.ref_branches[b] = self.repo.remotes[
                get_remote(settings, b)
            ].refs[s["follows"]]
            if b != "base-browser":
                self.browsers.append(b)
        self.moz_branch = self.repo.remotes[settings.moz_remote].refs[
            self.state["base-browser"]["moz-branch"]
        ]

    def run(self, push=True):
        # TODO: Run a range-diff!
        prev_base = self.branches["base-browser"].commit
        sanity_checks = {b: self._check_base(b) for b in self.browsers}
        bb_rebased = self._rebase_bb()
        # Try to rebase first, as it should be safe if we can.
        if bb_rebased:
            logger.debug(
                "Base Browser %s rebased, maybe rebasing also the others.",
                self.channel,
            )
            if push:
                self._push("base-browser")
            for b, passed in sanity_checks.items():
                if passed:
                    logger.info("Trying to rebase %s %s.", b, self.channel)
                    self._rebase_browser(b)
                    if push:
                        self._push(b)
                else:
                    logger.warning(
                        "Not rebasing %s %s because the pre-check failed.",
                        b,
                        self.channel,
                    )

        bb_cherry_picked = self._cherry_pick("base-browser")
        if bb_cherry_picked:
            self._shuffle_commits("base-browser", self.moz_branch)
            for b, passed in sanity_checks.items():
                self._rebuild_browser_branch(b, prev_base)

        for b in self.browsers:
            if self._cherry_pick(b):
                self._shuffle_commits(b, self.branches["base-browser"])
            if push:
                self._push(b)

    def _check_base(self, browser):
        """Check if browser is on top of base-browser."""

        bb_branch = self.branches["base-browser"]
        branch = self.branches[browser]
        bases = self.repo.merge_base(bb_branch, branch)
        if len(bases) != 1 or bases[0] != bb_branch.commit:
            logger.error(
                "%s %s is not a descendant of Base Browser! Merge bases: %s, expected %s.",
                browser,
                self.channel,
                bases,
                bb_branch.commit,
            )
            return False
        logger.debug("%s %s: pre sanity check passed", browser, self.channel)
        return True

    def _rebase_bb(self):
        """Rebase base-browser on top of upstream."""

        logger.info("Rebasing base-browser %s.", self.channel)
        branch = self.branches["base-browser"]
        commit_pre = branch.commit
        branch.checkout()
        self.repo.git.rebase(self.moz_branch)
        commit_post = branch.commit
        logger.debug(
            "Commits pre- and post- rebase: %s, %s", commit_pre, commit_post
        )
        if commit_pre == commit_post:
            logger.info("base-browser %s already up to date.", self.channel)
            return False
        else:
            logger.info("base-browser %s rebased.", self.channel)
            return True

    def _rebase_browser(self, browser):
        """Rebase browser on top of base-browser.

        This function should be called when a previous base-browser was
        already the ancestor of the browser, or rebase might add
        unwanted commits.
        """

        self.branches[browser].checkout()
        self.repo.git.rebase(self.branches["base-browser"])
        logger.info("%s %s rebased.", browser, self.channel)
        # No need to check if we updated, since we expected that our
        # base was updated and therefore the rebase was needed.

    def _cherry_pick(self, browser):
        """Cherry-pick new browser commits from tpo/applications."""

        self.branches[browser].checkout()
        last_seen = self.state[browser]["last-seen"]
        head = self.ref_branches[browser].commit.hexsha
        if head == last_seen:
            logger.debug(
                "%s %s: reference branch not updated since last time, no need to cherry-pick.",
                browser,
                self.channel,
            )
            return False
        logger.info(
            "%s %s: cherry-picking %s..%s",
            browser,
            self.channel,
            last_seen,
            head,
        )
        for c in self.repo.iter_commits(f"{last_seen}..{head}", reverse=True):
            try:
                cherry_pick(self.repo, c)
                self.state[browser]["last-seen"] = c.hexsha
            except Exception:
                logger.exception(
                    "Cannot cherry-pick %s on %s %s",
                    c.hexsha,
                    browser,
                    self.channel,
                )
                break
        logger.debug(
            "%s %s: old reference=%s, new reference=%s, wanted reference=%s",
            browser,
            self.channel,
            last_seen,
            self.state[browser]["last-seen"],
            head,
        )
        return self.state[browser]["last-seen"] != last_seen

    def _rebuild_browser_branch(self, browser, prev_base):
        """Rebase a browser on top of base-browser by cherry-picking.

        This is useful to be sure not to add extraneous commits.
        """

        logger.info(
            "Rebuilding %s %s with cherry-pick.", browser, self.channel
        )
        tmp_branch = self.repo.create_head(
            random_branch_name(browser), self.branches["base-browser"]
        )
        try:
            tmp_branch.checkout()
            for c in self.repo.iter_commits(
                f"{prev_base.hexsha}..{self.branches[browser].commit.hexsha}",
                reverse=True,
            ):
                cherry_pick(self.repo, c)
            self.branches[browser].checkout()
            self.repo.git.reset("--hard", tmp_branch)
        except Exception:
            logger.exception(
                "Failed to rebuild %s %s through cherry-picking.",
                browser,
                self.channel,
            )
        finally:
            self.branches["base-browser"].checkout()
            try:
                self.repo.delete_head(tmp_branch, force=True)
            except Exception:
                logger.exception(
                    "Failed to delete the temporary branch %s.", tmp_branch
                )

    def _shuffle_commits(self, browser, base):
        """Run a git rebase -i --autosquash, but do not really squash
        commits, just move them to the place where autosquash would
        put them.
        """
        logger.info("Shuffling commits for %s %s.", browser, self.channel)
        self.branches[browser].checkout()
        editor = f"{sys.executable} {__file__} --rebase-editor"
        logger.debug("Passing %s as an editor to git rebase -i", editor)
        with self.repo.git.custom_environment(EDITOR=editor):
            try:
                self.repo.git.rebase(base, interactive=True, autosquash=True)
                logger.debug(
                    "Shuffling %s %s succeeded.", browser, self.channel
                )
                return True
            except Exception as e:
                logger.warning("Failed to shuffle commits: %s", e)
                self.repo.git.rebase(abort=True)
                return False

    def _push(self, browser):
        """Force-push the browser to its tracking remote."""

        logger.info("Pushing %s %s", browser, self.channel)
        branch = self.branches[browser]
        tracking = branch.tracking_branch()
        self.repo.remotes[tracking.remote_name].push(branch, force=True)


def rebase_editor(instruction_file):
    with open(instruction_file, "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            tokens = line.split(" ")
            if line[0] == "#" or len(tokens) < 2 or tokens[0] == "pick":
                logger.debug(
                    "Git editor: outputting line as it is: %s", line.strip()
                )
                f.write(line)
            elif tokens[0] == "fixup" and tokens[1][0] == "-":
                logger.debug("Git editor: special fixup: %s", line.strip())
                f.write("pick " + " ".join(tokens[2:]))
            else:
                logger.debug("Git editor: fixup or squash: %s", line.strip())
                f.write("pick " + " ".join(tokens[1:]))
        f.truncate()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--recreate-on-build1", action="store_true")
    parser.add_argument("--reset-to-remote", action="store_true")
    parser.add_argument("--reset-to-build1")
    parser.add_argument("--rebase-editor")
    parser.add_argument("--channel")
    parser.add_argument(
        "--fetch", action=argparse.BooleanOptionalAction, default=True
    )
    parser.add_argument(
        "--push", action=argparse.BooleanOptionalAction, default=True
    )
    parser.add_argument(
        "--log-level",
        choices=["debug", "info", "warning", "error"],
        default="info",
        help="Set the log level",
    )
    args = parser.parse_args()

    log_level = getattr(logging, args.log_level.upper())
    logging.basicConfig(level=log_level)

    dir_ = Path(__file__).parent
    tool = RebaseTool(dir_ / "settings.json", dir_ / "state.json")
    if args.recreate_on_build1:
        tool.recreate_on_build1(args.channel)
    elif args.reset_to_remote:
        tool.reset_to_remote(args.channel)
    elif args.reset_to_build1:
        if not args.channel:
            print("--reset-to-build1 needs the channel.", file=sys.stderr)
            sys.exit(1)
        tool.reset_to_build1(args.channel, args.reset_to_build1)
    elif args.rebase_editor:
        rebase_editor(args.rebase_editor)
    elif args.channel:
        if args.fetch:
            tool.fetch()
        tool.run_channel(args.channel, args.push)
    else:
        tool.run(args.fetch, args.push)
