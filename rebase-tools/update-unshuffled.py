#!/usr/bin/env python3
import configparser
from random import randint
from time import time

from git import Repo
from git.exc import GitCommandError


class Branch:
    def __init__(self, name, conf):
        self.name = name
        self.worktree = Repo(conf["worktree"])
        if self.worktree.is_dirty():
            raise RuntimeError(f'Worktree {conf["worktree"]} is dirty!')
        self.branch = self.worktree.branches[name]
        self.esr_branch = conf["esr-branch"]
        self.channel = conf["channel"]
        self.follows = self.worktree.rev_parse(conf["follows"])
        self.last_seen = self.worktree.rev_parse(conf["last-seen"])
        # TODO: Implement
        self.shuffle = conf["shuffle"]

        slash = self.esr_branch.find("/")
        self.esr_remote = self.esr_branch[:slash]
        slash = conf["follows"].find("/")
        self.follows_remote = conf["follows"][:slash]

    def cherry_pick(self):
        if self.follows == self.last_seen:
            print(f"No need to cherry-pick on {self.name}")
            return

        print(f"Cherry-picking on {self.name}")
        self.branch.checkout()
        try:
            self.worktree.git.cherry_pick(f"{self.last_seen}..{self.follows}")
            self.last_seen = self.follows
        except GitCommandError as e:
            print(f"Failed to cherry-pick on {self.name}! Reverting!")
            print(e)
            self.worktree.git.cherry_pick(abort=True)

    def force_push(self):
        tracking = self.branch.tracking_branch()
        remote = self.worktree.remotes[tracking.remote_name]
        remote.push(self.name, force=True)


class Channel:
    def __init__(self, name):
        self.name = name
        self.branches = []
        self.repo = None
        self.base = None
        self.esr_branch = ""

    def add_branch(self, b):
        if self.esr_branch and b.esr_branch != self.esr_branch:
            raise ValueError(
                "All the branches in a channel must share the same ESR branch"
            )
        elif not self.esr_branch:
            self.esr_branch = b.esr_branch

        self.branches.append(b)
        if self.repo is None:
            self.repo = b.worktree

    def update(self):
        self._find_base()
        self._rebase_on_upstream()

    def _find_base(self):
        b0 = self.branches[0]
        for b in self.branches[1:]:
            bases = self.repo.merge_base(b0.branch, b.branch)
            if len(bases) != 1:
                raise RuntimeError("Expected exactly one base")
            if self.base is None:
                self.base = bases[0]
            elif self.base != bases[0]:
                raise RuntimeError(
                    f"Chennel {self.name} does not have a shared base!"
                )

    def _rebase_on_upstream(self):
        upstream_base = self.repo.merge_base(self.esr_branch, self.base)
        if len(upstream_base) != 1:
            raise RuntimeError(
                "Expected exactly one merge base with upstream."
            )
        if upstream_base[0] == self.repo.rev_parse(self.esr_branch):
            print(f"No need to rebase {self.name} on upstream")
            return

        # 2. Create a branch out of the common base and rebase it
        tmp_branch_name = (
            f"rebase-tool-tmp-{int(time())}-{randint(1000000, 9999999)}"
        )
        if tmp_branch_name in self.repo.branches:
            # Should never happen... Same time and same random int?
            raise RuntimeError("Our temporary branch already exists?!")

        print("Rebasing the base commit")
        tmp_branch = self.repo.create_head(tmp_branch_name, self.base)
        tmp_branch.checkout()
        self.repo.git.rebase(self.esr_branch)
        rebased_base = tmp_branch.commit
        print(f"Rebased. The new shared base is {rebased_base}.")

        # For each branch
        # a. Reset the temp branch to the rebased common base
        # b. Cherry-pick from the common base
        # c. Reset hard the branch to the cherry-picked
        for b in self.branches:
            print(f"Cherry-picking {b.name}")
            wt = b.worktree
            wt.branches[tmp_branch_name].checkout()
            wt.git.reset(rebased_base, hard=True)
            wt.git.cherry_pick(f"{self.base.hexsha}..{b.name}")
            b.branch.checkout()
            wt.git.reset(tmp_branch, hard=True)
            print(f"{b.name} done.")

        # Delete the temporary branch, we do not need it anymore
        print("Deleting the temporary branch")
        self.repo.delete_head(tmp_branch_name, force=True)


if __name__ == "__main__":
    status_file = "rebase-tool.ini"
    config = configparser.ConfigParser()
    config.read(status_file)

    branches = []
    channels = {}
    remotes = set()
    for name in config.sections():
        b = Branch(name, config[name])
        branches.append(b)
        if b.channel not in channels:
            channels[b.channel] = Channel(b.channel)
        channels[b.channel].add_branch(b)
        remotes.add(b.esr_remote)
        remotes.add(b.follows_remote)

    repo = branches[0].worktree
    for rem in remotes:
        print(f"Fetching {rem}")
        repo.remotes[rem].fetch()

    for ch in channels.values():
        ch.update()

    for b in branches:
        b.cherry_pick()
        config[b.name]["last-seen"] = b.last_seen.hexsha
        b.force_push()
    with open(status_file, "w") as f:
        config.write(f)
