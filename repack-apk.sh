#!/bin/bash
# Repack an APK after updating some files. Then align the resulting file and
# sign it again with the QA key.
# This script exists to update JavaScript files on an APK without having to go
# through Firefox's build system and gradle. It takes a few seconds instead of
# at least 1-1.5 minutes.
# It should be run in a directory prepared with the prepare function.
# TODO: Read arguments and expose a way to call prepare
set -e

prepare() {
  rm -rf apk omni
  apk="$(realpath "$1")"
  mkdir apk
  cd apk
  7z x "$apk"
  mkdir ../omni
  cd ../omni
  7z x ../apk/assets/omni.ja
}

repack() {
  rm -f repacked.apk aligned.apk apk/assets/omni.ja
  cd omni
  7z -tzip a ../apk/assets/omni.ja .
  cd ../apk
  # Some files cannot be compressed on R+.
  # Also, we want to be faaast, we don't care of size, so just copy
  7z -tzip a -mm=copy ../repacked.apk .
  cd ..
  "$zipalign" -p 4 repacked.apk aligned.apk
  "$apksigner" sign \
    --ks "$key" \
    --in aligned.apk \
    --out signed.apk \
    --ks-key-alias androidqakey \
    --key-pass pass:android \
    --ks-pass pass:android
}

apksigner="$(which apksigner)"
zipalign="$(which zipalign)"

if [[ "$1" == "--prepare" ]]; then
  prepare "$2"
  exit 0
fi

if [ -z "$apksigner" ] || [ -z "$zipalign" ]; then
  echo "apksigner and zipalign must be in your PATH"
  exit 1
fi

if [ -z "$key" ]; then
  key=~/Tor/tor-browser-build/projects/browser/android-qa.keystore
fi
if [ ! -f "$key" ]; then
  echo "This script needs a signing key"
  exit 1
fi

repack
# I've repacked an APK because I want to install it :P
# This assumes only one emulator is running an no actual Android device with ADB
# enabled is connected to your computer.
adb install -r signed.apk
