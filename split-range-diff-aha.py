#!/usr/bin/env python3
"""Process range-diff for commentary.

Usage:
`git range-diff --color R1 R2 | aha | split-range-diff-aha.py > file.html`.
"""

import html
import re
import sys
import warnings

from bs4 import BeautifulSoup, MarkupResemblesLocatorWarning


subj_re = re.compile(r"[\d-]+:\s+[\da-f-]+\s+([=!<>])\s+[\d-]+:\s+[\da-f-]+.*")
class_ = ""
lines = []

# https://stackoverflow.com/a/78445796
warnings.filterwarnings("ignore", category=MarkupResemblesLocatorWarning)


print(
    """<!DOCTYPE html>
<html>
<title>range-diff</title>
<meta charset="utf-8">
<style>
html { font-family: Roboto,sans-serif; }
pre, code { font: 1.2rem Inconsolata,monospace; }
details { margin: 1em 0; }
pre {
  margin: 1em;
  padding: 1em;
  border: 1px dashed gray;
  border-width: 1px 0;
  overflow: auto;
}
.equal summary { color: #999999; }
.changed summary { color: #E8AC41; }
.need-attention summary { color: #FD673A; }
</style>
</head>
<body>
"""
)


def print_lines():
    global lines
    print(f'<details class="{class_}">')
    print("\n".join(lines))
    print("</pre></details>")
    lines = []


while True:
    line = sys.stdin.readline()
    # readline always includes a \n, except if it was EOF.
    # See https://stackoverflow.com/a/69396696
    if not line:
        break
    line = line.strip()
    if line == "<body>":
        break

while True:
    line = sys.stdin.readline()
    if not line:
        break
    line = line.rstrip()
    if line == "<pre>":
        continue

    soup = BeautifulSoup(line, "html.parser")
    plain_text = soup.get_text()

    # No spaces! It's a new commit.
    if line.startswith("<span"):
        if lines:
            print_lines()

        subj = html.escape(plain_text.strip())
        action = subj_re.match(plain_text.strip()).groups(1)[0]
        if action == "=":
            class_ = "equal"
        elif action == "!":
            class_ = "changed"
        else:
            class_ = "need-attention"
        lines = lines = [f"<summary>{subj}</summary>", "<pre>", line]
    elif line == "</pre>":
        print_lines()
        break
    elif not lines:
        # Immediately print if we have not started a `<details>` yet.
        print(line)
    else:
        action = plain_text[1:3]
        if action in ["++", "+-", "--", "-+"]:
            class_ = "need-attention"
        lines.append(line)

while True:
    line = sys.stdin.readline()
    if not line:
        break
    print(line)
