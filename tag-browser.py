#!/usr/bin/env python3
import re
import subprocess
import sys

if len(sys.argv) >= 2:
    tag = sys.argv[1]
    m = re.match(r"\w+-browser-([^-]+)-([\d\.]+)-.+-(build\d+)", tag)
    if not m:
        print("Cannot match the tokens of the tag")
        sys.exit(1)
    build = m[3]
else:
    branch = subprocess.run(
        ["git", "symbolic-ref", "--short", "HEAD"],
        capture_output=True,
        text=True,
    ).stdout.strip()
    m = re.match(r"\w+-browser-([^-]+)-([\d\.]+)-\d+", branch)
    if not m:
        print("Tag not specified, and could not detect it automatically.")
        print(f"Usage: {sys.argv[0]} tag [git-tag options]")
        sys.exit(1)
    tags = subprocess.run(
        ["git", "tag", "-l", f"{branch}-build*"],
        capture_output=True,
        text=True,
    ).stdout.strip()
    tags = [] if not tags else tags.split("\n")
    build = f"build{len(tags) + 1}"
    tag = f"{branch}-{build}"

if m[2] == "13.5" and not branch.startswith("mullvad-"):
    channel = "legacy"
elif m[2] in ("14.0", "13.5"):
    channel = "stable"
else:
    channel = "alpha"
message = f"Tagging {build} for {m[1]}-based {channel}"
print(message)
subprocess.run(["git", "tag", "-as", tag, "-m", message] + sys.argv[2:])
