#!/bin/bash
# This script goes through the process of building GeckoView for x86_64 (to use
# in the emulator), publish it to the local Maven repository (~/.m2) and then
# use it to build Fenix.
# Finally, it signs the build with our QA keys and install it on the emulator.
# This assumes lazy-script is at the same level of a tor-browser.git cloned into
# a tor-browser directory, and of a firefox-android.git cloned into a
# firefox-android directory.
# It also assumes any needed change to $PATH has already been made.
# Also, the script works only for incremental builds, so any clobber or
# configure needs to be done before running it.
set -e

cd "$(dirname "$0")/.."

export MOZCONFIG=mozconfig-android-x86_64

if [[ -z "$LOCAL_DEV_BUILD" ]]; then
  # The magic file is pierenvironment.fish.
  echo "Have you sourced your magic file?"
  exit 1
fi

cd tor-browser
if [[ "$1" = "--clobber" ]]; then
  ./mach clobber
  ./mach configure \
    --enable-update-channel=nightly \
    --with-base-browser-version=testbuild
fi

./mach build
./mach package-multi-locale --locales en-US $MOZ_CHROME_MULTILOCALE

cd mobile/android/fenix
gradle --no-daemon -Dorg.gradle.jvmargs=-Xmx20g -PdisableOptimization assembleNightly
tools/tba-sign-devbuilds.sh
# Assumption: only one emulator is running and no real Android device with
# debugging enabled is connected.
adb install -r app/build/outputs/apk/fenix/nightly/app-fenix-x86_64-nightly-signed.apk

